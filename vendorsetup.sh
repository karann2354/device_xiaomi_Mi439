# Clone HALs

rm -rf hardware/qcom-caf/wlan

git clone https://github.com/karann2354/msm8953_hals -b wlan hardware/qcom-caf/wlan
git clone https://github.com/karann2354/msm8953_hals -b display hardware/qcom-caf/msm8953/display
git clone https://github.com/karann2354/msm8953_hals -b audio hardware/qcom-caf/msm8953/audio
git clone https://github.com/karann2354/msm8953_hals -b media hardware/qcom-caf/msm8953/media

# Radio

rm -rf packages/apps/FMRadio

git clone https://github.com/karann2354/packages_apps_FMRadio packages/apps/FMRadio

# MIUI Camera

git clone https://github.com/karann2354/vendor_miuicamera vendor/miuicamera --depth=1

# Patches

rm -rf vendor/superior
rm -rf system/core

git clone https://github.com/karann2354/android_vendor_superior vendor/superior --depth=1
git clone https://github.com/karann2354/system_core system/core
