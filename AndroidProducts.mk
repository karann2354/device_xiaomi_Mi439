#
# Copyright (C) 2021 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/superior_Mi439.mk

COMMON_LUNCH_CHOICES := \
    superior_Mi439-user \
    superior_Mi439-userdebug \
    superior_Mi439-eng
